window.onload = init;

function init() {
    const paragraphs = document.getElementsByTagName("p");
    const imgs = document.querySelectorAll("img");

    for (let i = 0; i < paragraphs.length; i++) {
        paragraphs[i].innerHTML = "Puppy # "+ (i+1);
    }

    Array.from(paragraphs).forEach(function (element) {
        element.style.border = "2px solid black";
        });


    for (let i = 0; i < imgs.length; i++) {
        imgs[i].alt = "puppy " + (i+1);
        imgs[i].src = "/images/puppy" + (i+1) + ".jfif";
        imgs[i].classList.add("roundedImg");
    }

}