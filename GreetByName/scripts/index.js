"use strict"

window.onload = init;

function init() {
    const greetBtn = document.getElementById("greetBtn");
    greetBtn.onclick = onGreetUserBtnClicked;

}

function onGreetUserBtnClicked () {
    const nameFieldValue = document.getElementById("nameField").value;
    const para = document.getElementById("para");

    let message = `Hello ${nameFieldValue}.`;
    para.innerHTML = message;
    alert(message);
}

