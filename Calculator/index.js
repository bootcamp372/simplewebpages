"use strict"

window.onload = init;
function init() {
    const helloBtn = document.getElementById("helloBtn");
    addBtn.onclick = onAddBtnClicked;
    subtractBtn.onclick = onSubtractBtnClicked;
    multiplyBtn.onclick = onMultiplyClicked;
    divideBtn.onclick = onDivideBtnClicked;


}

function onAddBtnClicked () {
    const numOneinput = document.getElementById("numOne");
    const numTwoinput = document.getElementById("numTwo");
    const answerEl = document.getElementById("answer");

    let numOne = Number(numOneinput.value);
    let numTwo = Number(numTwoinput.value);
    let answer = numOne + numTwo;

    answerEl.value = answer;
}

function onSubtractBtnClicked () {
    const numOneinput = document.getElementById("numOne");
    const numTwoinput = document.getElementById("numTwo");
    const answerEl = document.getElementById("answer");

    let numOne = Number(numOneinput.value);
    let numTwo = Number(numTwoinput.value);
    let answer = numOne - numTwo;

    answerEl.value = answer;
}

function onMultiplyClicked () {
    const numOneinput = document.getElementById("numOne");
    const numTwoinput = document.getElementById("numTwo");
    const answerEl = document.getElementById("answer");

    let numOne = Number(numOneinput.value);
    let numTwo = Number(numTwoinput.value);
    let answer = numOne * numTwo;

    answerEl.value = answer;
}

function onDivideBtnClicked () {
    const numOneinput = document.getElementById("numOne");
    const numTwoinput = document.getElementById("numTwo");
    const answerEl = document.getElementById("answer");

    let numOne = Number(numOneinput.value);
    let numTwo = Number(numTwoinput.value);
    let answer = numOne / numTwo;

    answerEl.value = answer;
}

