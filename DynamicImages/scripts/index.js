let imageFiles = [{
        name: "/DynamicImages/images/DAL.png",
        description: "Dallas Cowboys logo"
    },
    {
        name: "/DynamicImages/images/KAN.png",
        description: "Kansas City Cheifs logo"
    },
    {
        name: "/DynamicImages/images/puppy1.jfif",
        description: "Puppy One"
    },
    {
        name: "/DynamicImages/images/puppy2.jfif",
        description: "Puppy Two"
    },
    {
        name: "/DynamicImages/images/puppy3.jfif",
        description: "Puppy Three"
    }
];

window.onload = function () {
    const clearBtn = document.getElementById("clearBtn");
    const addImgBtn = document.getElementById("addImgBtn");
    const imgList = document.getElementById("imgList");

    initDropdown();

    //imgList.onchange = changeImg;
    addImgBtn.onclick = addImg;
    clearBtn.onclick = clearValues;

};

function initDropdown() {
    const imgList = document.getElementById("imgList");
    let imageFilesLength = imageFiles.length;

    let selectOption = new Option("Select an image", "");
    imgList.appendChild(selectOption);

    for (let i = 0; i < imageFilesLength; i++) {
        let imgOption = new Option(imageFiles[i].name, imageFiles[i].description);
        imgList.appendChild(imgOption);
    }

}

function addImg() {
    const selection = document.getElementById("imgList").value;
   // const selection = document.getElementById("imgList").selectedIndex;
    const imgDiv = document.getElementById("imgDiv");
    // using selected index, define images this way
    // img.src = selection.name;
    // img.alt = selection.description;

    let imageFilesLength = imageFiles.length;
    // add image based on selection

    for (let i = 0; i < imageFilesLength; i++) {
        if (imageFiles[i].description === selection) {
            let img = document.createElement("img");
            img.src = imageFiles[i].name;
            img.alt = imageFiles[i].description;
            imgDiv.appendChild(img);
        }

    }

}

function clearValues() {

    document.getElementById("imgDiv").innerHTML = "";
}