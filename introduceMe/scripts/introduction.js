function clickMe() {

    // values of inputs
    const formName = document.getElementById('formName').value;
    const hobby = document.getElementById('hobby').value;
    const color = document.getElementById('color').value;
    const currentCoffee = document.getElementById('currentCoffee').value;
    const maxCoffee = document.getElementById('maxCoffee').value;

    //message that can change about variables above
    let messageOne = `${formName}'s hobby is ${hobby}. ${formName}'s favorite color is ${color}. ${formName} is allowed ${maxCoffee} cups of coffee. ${formName} has already consumed ${currentCoffee} cups of coffee.`;

    //calculation of how many cups of coffee can still be consumed
    let cupsLeft = maxCoffee - currentCoffee;

    //message for how many cups of coffee are left
    let messageTwo = ` ${formName} can consume ${cupsLeft} more cups of coffee today.`;

    //display messages as html
    document.getElementById('messageDiv').innerHTML = messageOne + messageTwo;

    //change font color of messageDiv
    document.getElementById('messageDiv').style.color = color.toLowerCase();


}